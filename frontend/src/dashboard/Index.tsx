import { Row, Col, Form, Container, Table } from 'react-bootstrap';
import EventsTable from './EventsTable';
import { useState } from 'react'

export default function DashboardIndex() {
  const [recipientId, setRecipientId] = useState<string | null>(null)

  return (
  <Container>
    <h3 className='mt-2'>Last 30 activities</h3>
    <Row className='mt-2'>
      <Col xs='4'>
        <Form.Select aria-label="Choose Care Recepient" onChange={ (event) => setRecipientId(event.target.value) }>
          <option>Choose Care Recepient</option>
          <option value="df50cac5-293c-490d-a06c-ee26796f850d">df50cac5-293c-490d-a06c-ee26796f850d</option>
          <option value="ad3512a6-91b1-4d7d-a005-6f8764dd0111">ad3512a6-91b1-4d7d-a005-6f8764dd0111</option>
          <option value="e3e2bff8-d318-4760-beea-841a75f00227">e3e2bff8-d318-4760-beea-841a75f00227</option>
        </Form.Select>
      </Col>
    </Row>
    <Table />
    <EventsTable recipientId={recipientId}/>
  </Container>
  )
}