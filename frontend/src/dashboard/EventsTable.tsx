import { Row, Col, Table } from 'react-bootstrap';
import { useEffect, useState } from 'react'
import axios from 'axios'

export default function EventsTable({ recipientId }: any){
  const [events, setEvents] = useState([])

  useEffect(() => {
    async function fetchEvents() {
      const items = await axios.get(`/events?filter_by_recipient_id=${recipientId}`).then(response => response.data)
      setEvents(items)
    }
    fetchEvents()
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [recipientId])

  if (events.length === 0) {
    return (
      <Row className='mt-2'><Col>There are no activities, please choose care recipient.</Col></Row>
    )
  }

  const EventsRows = events.map((event: { visit_id: string, caregiver_id: string, event_type: string, timestamp: string }, index) => {
    return (
      <tr key={index}>
        <td>{ event.visit_id }</td>
        <td>{ event.caregiver_id }</td>
        <td>{ event.event_type }</td>
        <td>{ event.timestamp } </td>
      </tr>
    )
  })

  return (
    <Row className='mt-2'>
      <Col>
        <Table striped bordered hover>
          <thead>
            <tr>
              <th>Visit ID</th>
              <th>Caregiver ID</th>
              <th>Event</th>
              <th>Data</th>
            </tr>
          </thead>
          <tbody>
            { EventsRows }
          </tbody>
        </Table>
      </Col>
    </Row>
  )
}