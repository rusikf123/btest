import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import DashboardIndex from './dashboard/Index';

function App() {
  return (
    <div className="App">
      <DashboardIndex />
    </div>
  );
}

export default App;
