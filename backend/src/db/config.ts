import { Dialect, Sequelize } from 'sequelize'
require('dotenv').config()

const dbName = process.env.DB_NAME as string
const dbUser = process.env.DB_USER as string
const dbHost = process.env.DB_HOST as string
const dbDriver = 'mysql' as Dialect
const dbPassword = process.env.DB_PASSWORD as string
const dbPort = process.env.DB_PORT as any

const sequelizeConnection = new Sequelize(dbName, dbUser, dbPassword, {
  host: dbHost,
  port: dbPort,
  dialect: dbDriver
})

export default sequelizeConnection