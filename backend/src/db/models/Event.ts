import { DataTypes, Model, Optional } from 'sequelize'
import sequelizeConnection from '../config';

interface EventAttributes {
  id: number;
  event_type: string;
  visit_id: string;
  caregiver_id: string;
  care_recipient_id: string;
  timestamp: string;
}

export interface EventInput extends Optional<EventAttributes, 'id'> {}

export interface EventOutput extends Required<EventAttributes> {}

class Event extends Model<EventAttributes, EventInput> implements EventAttributes {
  public id!: number;
  public event_type!: string;
  public visit_id!: string;
  public caregiver_id!: string;
  public care_recipient_id!: string;
  public timestamp!: string;
}

Event.init({
  id: {
    type: DataTypes.STRING,
    primaryKey: true,
  },
  event_type: {
    type: DataTypes.STRING,
    allowNull: false
  },
  visit_id: {
    type: DataTypes.STRING,
    allowNull: false
  },
  caregiver_id: {
    type: DataTypes.STRING,
    allowNull: false
  },
  care_recipient_id: {
    type: DataTypes.STRING,
    allowNull: false
  },
  timestamp: {
    type: DataTypes.STRING,
    allowNull: false
  }
}, {
  sequelize: sequelizeConnection,
  modelName: 'event',
  timestamps: false
})

export default Event