import * as express from "express";
import { eventsController } from "./controllers/events"

const app = express();

const path = require("path");
const frontend = path.resolve(__dirname + "/../../frontend/build")
console.log('path to fe', frontend);
app.use('/', express.static(frontend));


// TODO: use it only in development
app.use(function (_: any, res: any, next: any) {

  // Website you wish to allow to connect
  res.setHeader('Access-Control-Allow-Origin', 'http://localhost:3001');

  // Request methods you wish to allow
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

  // Request headers you wish to allow
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

  // Set to true if you need the website to include cookies in the requests sent
  // to the API (e.g. in case you use sessions)
  res.setHeader('Access-Control-Allow-Credentials', 'true');

  // Pass to next layer of middleware
  next();
});

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(eventsController);
export default app;
