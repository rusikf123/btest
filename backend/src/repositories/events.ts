import { Event } from '../db/models'
const LIMIT = 30

export default class EventsRepository {
  async list(care_recipient_id: string) {
    return await Event.findAll({
      where: { care_recipient_id },
      order: [['timestamp', 'DESC']], limit: LIMIT, raw: true })
  }
}