import * as express from "express";
import Repository from '../repositories/events'

export const eventsController = express.Router();

eventsController.get('/events', async (req: any, res) => {
  const repo = new Repository()
  const { filter_by_recipient_id } = req.query

  if (!filter_by_recipient_id) {
    return res.status(422).json({ errors: ['filter_by_recipient_id is required'] })
  }

  const users = await repo.list(filter_by_recipient_id)
  return res.status(200).json(users)
});


