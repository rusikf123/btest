import app from '../src/application'
import * as request from 'supertest';
import sequelize from '../src/db/config'

describe('/events', () => {
  afterAll(async () => {
    await sequelize.close()
  })

  it('filter_by_recipient_id parameter exist', async () => {
    const response = await request(app)
      .get('/events?filter_by_recipient_id=df50cac5-293c-490d-a06c-ee26796f850d')
      .expect(200)

    expect(response.body.length).toEqual(30)
  });

  it('return 422 if no filter items', async () => {
    await request(app)
      .get('/events')
      .expect(422)
  });
});
